This project performs authentication of credit/debit card using Face Recognition.

It uses Tkinter to create GUI which can be is used for two different perspective. First is used by corporate users to register their customers. Second is used by customers to perform transaction from their users through face recognition.

Describing first use case, Corporate users log into their protal through their ID and password which is unique and then captures the their customer's personal and card details and stored it in sql database. At least 50 frames of user’s face are also captured and are stored it in training images folder created. Then these images are trained using haarcascade code and LBPH face recognizer and then is linked with the user’s card credentials.

Describing second use case OpenCV and LBPH face recognition is used to detect face of user and verify the image captured with that stored in our database, to proceed for payment which eliminates the use of card/otp/cvv to make payments
